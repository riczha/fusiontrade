Transactions = new Mongo.Collection('transactions');


/*

Date   :   date //    date that transaction is executed
symbol :   string //  security code
isBid  :   boolean // is a buy / sell transaction
price  :   trade price
volume :   trade volume

 */