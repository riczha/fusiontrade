Portfolios = new Mongo.Collection('portfolios');


Portfolios.allow({
    insert: function (userId, portfolio) {
        return userId;
    },
    update: function (userId, portfolio, fields, modifier) {
        return userId && portfolio.owner === userId;
    },
    remove: function (userId, portfolio) {
        return userId && portfolio.owner === userId;
    }
});


/*
    owner: user id
    balance : number // available fund to invest
    assets  : [{
        code : string   // security symbol,
        volume : number // number of units bought for this security
        tradePrice : number // price that user bought the security
        tradeDate : date
    }]     // list of securities
 */