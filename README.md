# Development Overview:

After some consideration, I decided to use MeteorJs to implement this demo task.
Meteor is a framework built on top of NodeJs which can provide good reactive env and support realtime applications.



# Prerequisites

Pretty much only NodeJs, MeteorJs and MongoDB is needed.

On production env, meteor is not even needed. a prebuilt package can just run in NodeJs platform.


# Installation:

https://www.meteor.com/install

### On Mac or Linux:

curl https://install.meteor.com/ | sh

### On Windows:

Good luck :)


# Run application:

You can start the app by run meteor from command line and you can also specify a particular port to start it as following:

meteor --port 10025

If you don't want Meteor to use its default Mongo instance, you can just set an environment variable to set the MONGO url

export MONGO_URL=mongodb://localhost:27017/fusion

With this option, you can simply check your data from mongo's normal shell.



Otherwise, meteor will start its own instance to store the data.
You can run a command to check the data :
meteor mongo



# Structures of the application:


   - client : all client side code, mostly Angular component / controller

   - model  : database collection definitions

   - server : provide Meteor method/publish functions

   - packages : I followed the convention to separate the browser code and mobile code into two different packages even I
              did not have mobile implementation yet but Meteor provides pretty clear logic to share/separate code between
              browser and mobile


# There are four views implemented so far

   - My portfolio : shows account balance and stocks currently hold.

   - Transaction history : a history or trading records

   - Watchlist : This provides a watch list of all stocks that I want to view. This is a real-time quotes at 3 seconds
     interval. I run a meteor cron job to collect the realtime quotes every 3 seconds.

   - Historical Chart view : from each stock in the watch list, you can click the stock to view the historical chart.


# DEMO  

 
   [Demo Website](http://fusion.jeaniusdesign.com.au)
   
   To login, please use following user/pass

   - email : john.doe@hotmail.com
   - pass :  temp123



# Notes :

- Watchlist can achieve realtime update.
- Instant search in transaction history view can reactively search transactions by stock symbol
- Buy/Sell shares will use the current sell price/ buy price retrieved from Yahoo finance
- In the portolio view, the market value is calculated by using last trade price from Yahoo. Same for the total asset value on the top
- The historical graph looks pretty good. I used high chart to plot the data I received from Yahoo Finance API



# Limitations

- the external api sometimes might block the application due to network issue. User might want to re-submit the order if it is blocked in the back end.
  This is supposed to be handled which can provide better user
  experiences. For example, some loading icon, or proper user message.

- some views are not very responsive. mainly when table has a lot of columns, it looks ugly on small screens. a proper media query solution might
  help.

- I did not have time to create unit tests  and test thoroughly yet, so bugs !






-









