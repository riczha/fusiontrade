'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('fusion.browser')
    .directive('sidebarSearch',function() {
        return {
            templateUrl:'/packages/fusion-browser/client/fusion/sidebar/sidebar-search.ng.html',
            restrict: 'E',
            replace: true,
            scope: {
            },
            controller:function($scope){
                $scope.selectedMenu = 'home';
            }
        }
    });
