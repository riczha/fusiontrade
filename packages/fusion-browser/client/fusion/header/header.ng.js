'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('fusion.browser')
    .directive('fusionHeader',function(){
        return {
            templateUrl:'/packages/fusion-browser/client/fusion/header/header.ng.html',
            restrict: 'E',
            replace: true,
        }
    });


