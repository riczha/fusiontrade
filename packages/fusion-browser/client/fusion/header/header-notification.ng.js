'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('fusion.browser')
    .directive('headerNotification',function(){
        return {
            templateUrl:'/packages/fusion-browser/client/fusion/header/header-notification.ng.html',
            restrict: 'E',
            replace: true,
        }
    });


