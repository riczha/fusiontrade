angular.module("fusion.browser").directive('login', function () {
    return {
        restrict: 'E',
        templateUrl: '/packages/fusion-browser/client/auth/login/login.ng.html',
        controllerAs: 'login',
        controller: function ($scope, $reactive, $state) {
            $reactive(this).attach($scope);

            this.credentials = {
                nameOrEmail: '',
                password: ''
            };

            this.error = '';

            this.login  = function () {

                if (!this.credentials.nameOrEmail || !this.credentials.password)
                {
                    return;
                }
                Meteor.loginWithPassword(this.credentials.nameOrEmail, this.credentials.password, function (err) {

                    if (err) {
                        console.log('Login error - ', err);
                    }
                    else {
                        console.log('Login success');
                        $state.go("app.portfolio");
                    }

                });
            }
        }
    }
});
