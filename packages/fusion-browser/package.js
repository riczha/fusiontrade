Package.describe({
  name: 'fusion-browser',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use('angular');
  api.use('less');


  api.addFiles([
    'client/lib/module.js',
    'client/auth/login/login.component.js',
    'client/auth/login/login.ng.html',
    'client/fusion/header/header.ng.html',
    'client/fusion/header/header-notification.ng.html',
    'client/fusion/header/header.ng.js',
    'client/fusion/header/header-notification.ng.js',
    'client/fusion/sidebar/sidebar.ng.html',
    'client/fusion/sidebar/sidebar.ng.js',
    'client/fusion/sidebar/sidebar-search.ng.html',
    'client/fusion/sidebar/sidebar-search.ng.js',
    'client/fusion/base.ng.html',
    'client/portfolio/details.ng.html',
    'client/watchlist/list.ng.html',
    'client/transactions/list.ng.html',
    'client/chart/chart.ng.html'
  ],'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('fusion-browser');
  api.addFiles('');
});
