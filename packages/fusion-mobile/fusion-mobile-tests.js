// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by fusion-mobile.js.
import { name as packageName } from "meteor/fusion-mobile";

// Write your tests here!
// Here is an example.
Tinytest.add('fusion-mobile - example', function (test) {
  test.equal(packageName, "fusion-mobile");
});
