Meteor.methods({

    getTicker: function (symbol) {
        var data = YahooFinance.snapshot({
            symbols: [symbol],
            fields: ['n', 'a', 'b']
        });

        return data;
    },

    getHistoricalData: function (symbol) {

        if (symbol) {

            var end = new Date();
            var start = new Date(end);
            start.setDate(start.getDate() - 365);

            var snapshots = YahooFinance.historical({
                symbol: symbol,
                from: start,
                to: end
            });

            return snapshots;
        }

        return [];
    },

    /*
     // Date
     c1: 'Change',
     c: 'Change And Percent Change',
     c6: 'Change (Realtime)',
     k2: 'Change Percent (Realtime)',
     p2: 'Change in Percent',
     d1: 'Last Trade Date',
     d2: 'Trade Date',
     t1: 'Last Trade Time',
     */
    getWatchListQuotes: function () {

        var cursor = Portfolios.find({});

        cursor.forEach(function(portfolio){
            // Delete all existing songs from database
            if (portfolio.watchlist.length > 0 || portfolio.assets.length > 0) {

                var symbols = portfolio.watchlist ? portfolio.watchlist : [];

                portfolio.assets.forEach(function(asset){
                    if (symbols.indexOf(asset.code) < 0)
                    {
                        symbols.push(asset.code);
                    }
                });

                var rawData = YahooFinance.snapshot({
                    symbols: symbols,
                    fields: ['s', 'n','a', 'b','o','g','h','p2','l1','v']
                });

                if (rawData && rawData.length > 0) {
                    Quotes.remove({});
                    rawData.forEach(function (quote) {
                        Quotes.insert(quote);
                    });
                }
            }
        });
    },

    updateBalance: function (amount) {

        if (Portfolios.find().count({owner: this.userId}) == 0) {
            Portfolios.insert({
                owner: this.userId,
                balance: 0, // available fund to invest
                assets: [],
                watchlist : []
            });   // list of securities
        }

        var portfolio = Portfolios.findOne({owner: this.userId});


        if (portfolio) {
            var newBalance = amount + portfolio.balance;
            if (newBalance < 0) {
                throw new Meteor.Error(404, "Insufficient funds to withdraw");
            }

            Portfolios.update(portfolio._id, {$set: {balance: newBalance}});
            return true;
        }

        return false;
    },


    buyShare: function (order) {

        var portfolio = Portfolios.findOne({owner: this.userId});

        if (portfolio) {

            var data = YahooFinance.snapshot({
                symbols: [order.code],
                fields: ['n', 'a']
            });

            if (!data || data.length == 0)
                throw new Meteor.Error(404, "Invalid Stock Code");

            if (!data[0].ask) {
                throw new Meteor.Error(404, "No ask on the market!");
            }

            if ((data[0].ask * order.volume) > portfolio.balance) {
                throw new Meteor.Error(404, "Transaction failed : Insufficient fund");
            }


            var assetCursor = portfolio.assets.filter(
                function (asset) {
                    return asset.code.toUpperCase() === order.code.toUpperCase();
                }
            );

            var newAsset = {
                code: order.code.toUpperCase(),
                volume: 0,
                cost :0
            };

            if (assetCursor && assetCursor.length > 0) {
                newAsset.volume += assetCursor[0].volume;
                newAsset.cost += assetCursor[0].cost;
                Portfolios.update(portfolio._id, {$pull: {assets:{code : order.code.toUpperCase() }}},{multi: true});
            }

            newAsset.volume += order.volume;
            newAsset.cost += order.volume * data[0].ask;

            Portfolios.update(portfolio._id, {$push: {assets: newAsset}});
            Portfolios.update(portfolio._id, {$inc:  {balance: data[0].ask * order.volume * -1}});

            Transactions.insert({
                symbol: order.code.toUpperCase(),
                price: data[0].ask,
                volume: order.volume,
                isBid: true
            });

            return true;
        }
        else {
            throw new Meteor.Error(404, "Transaction failed : No account created yet. Please deposit to open your account");
        }

        return false;
    },

    sellShare: function (order) {

        var portfolio = Portfolios.findOne({owner: this.userId});

        if (portfolio) {

            console.log('received order : ' + order.code);
            var data = YahooFinance.snapshot({
                symbols: [order.code],
                fields: ['n', 'b']
            });

            if (!data || data.length == 0)
                throw new Meteor.Error(404, "Invalid Stock Code!");

            if (!data[0].bid) {
                throw new Meteor.Error(404, "No bid on the market!");
            }

            var assetCursor = portfolio.assets.filter(
                function (asset) {
                    return asset.code.toUpperCase() === order.code.toUpperCase();
                }
            );

            if (!assetCursor || assetCursor.length == 0 || order.volume > assetCursor[0].volume) {
                throw new Meteor.Error(404, "User doesn't have enough to sell on this stock! ");
            }

            Transactions.insert({
                symbol: order.code.toUpperCase(),
                price: data[0].bid,
                volume: order.volume,
                isBid: false
            });

            var newAsset = {
                code: order.code.toUpperCase(),
                volume: (assetCursor[0].volume - order.volume),
                cost: assetCursor[0].cost
            };

            Portfolios.update(portfolio._id, {$pull: {assets: {code : order.code.toUpperCase()}}},{multi: true});

            if (order.volume < assetCursor[0].volume) {
                {
                    Portfolios.update(portfolio._id, {$push: {assets: newAsset}});
                }
            }

            Portfolios.update(portfolio._id, {$inc: {balance: data[0].bid * order.volume}});
            return true;
        }

        return false;
    },

    addToWatch: function (symbol) {

        if (Portfolios.find().count({owner: this.userId}) == 0) {
            Portfolios.insert({
                owner: this.userId,
                balance: 0, // available fund to invest
                assets: [],
                watchlist : []
            });   // list of securities
        } // todo, this would not be necessary for the real product. just for quick demo purpose now.

        var portfolio = Portfolios.findOne({owner: this.userId});

        if (portfolio && symbol) {

            var code = symbol.toUpperCase();

            if (portfolio.watchlist.indexOf(code) >= 0) {
                console.log('already has in watchlist');
                throw new Meteor.Error(404, "Stock is already in watchlist ");
            }

            Portfolios.update(portfolio._id, {$push: {watchlist: code}});
        }
        else {
            throw new Meteor.Error(404, "Unknown error when we are trying to add symbol to your watch list ");
        }
    }


});
