var createUser = function (userData) {
    var id = Accounts.createUser(userData);
}

// Setup cron jobs
SyncedCron.options.collectionName = 'cronjobs';

SyncedCron.add({
    name: 'Real time Quotes',
    schedule: function(parser) {
        return parser.text('every 3 seconds'); // parser is a later.parse object
    },
    job: function() {
        Meteor.call('getWatchListQuotes');
    }
});

Meteor.startup(function () {

    if (Meteor.users.find().count() == 0) {
        var user = {
            username: 'john',
            email: 'john.doe@hotmail.com',
            password: 'temp123',
            profile: {
                name: 'John Doe'
            }
        };

        createUser(user);
    }

    SyncedCron.start();
});