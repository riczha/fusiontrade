
Transactions.before.insert(function(userid, transaction){
    //add or overwrite the createdAt property for each insert
    //add or overwrite the createdAt property for each insert
    var today = new Date();
    transaction.date = new Date(today.getTime());
});


Meteor.publish('transactions', function (options, keyword) {

    if (keyword == null)
        keyword = '';

    Counts.publish(this, 'numberOfTransactions', Transactions.find({
        'symbol': {'$regex': '.*' + keyword || '' + '.*', '$options': 'i'}
    }), {noReady: true});

    return Transactions.find({
        'symbol': {'$regex': '.*' + keyword || '' + '.*', '$options': 'i'}
    }, options);

});