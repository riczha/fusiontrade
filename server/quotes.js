Meteor.publish('quotes', function (options, keyword) {

    if (keyword == null)
        keyword = '';

    Counts.publish(this, 'numberOfQuotes', Transactions.find({
        'symbol': {'$regex': '.*' + keyword || '' + '.*', '$options': 'i'}
    }), {noReady: true});

    return Quotes.find({
        'symbol': {'$regex': '.*' + keyword || '' + '.*', '$options': 'i'}
    }, options);
});