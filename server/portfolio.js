// TODO For now, we only support one Portfolio per user
// however this can be extended to support multiple portfolios per user. e.g one user might have a couple of
// trading account
Meteor.publish('portfolios', function (options, keyword) {

    if (keyword == null)
        keyword = '';

    var selector = [
        {owner: this.userId},
        {owner: {$exists: true}}
    ];

    //Counts.publish(this, 'numberOfPortfolios', Portfolios.find({
    //    $and: selector
    //}), {noReady: true});

    return Portfolios.find({
        $and: selector
    }, options);

});