angular.module("fusion").controller("BalanceModalCtrl", ['$meteor','$scope', '$modalInstance','$reactive',
    function ($meteor,$scope, $modalInstance,$reactive) {

        $reactive(this).attach($scope);

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        }

        $scope.deposit = function()
        {

            $scope.error  = null;
            Meteor.call("updateBalance", $scope.amount, function(error, result){
                if (error) {
                    console.log(error);
                    $scope.error = error.message;
                }
                else{
                    $modalInstance.close();
                }
            });

        };

        $scope.withdraw = function()
        {
            Meteor.call("updateBalance", $scope.amount * -1, function(error, result){
                if (error) {
                    console.log(error);
                    $scope.error = error.message;
                }
                else{
                    $modalInstance.close();
                }
            });
        };
    }
]);
