angular.module("fusion").controller("TradeModalCtrl", ['$meteor','$scope', '$modalInstance','$reactive', 'portfolio','asset',

    function ($meteor,$scope, $modalInstance,$reactive, portfolio, asset) {

        $reactive(this).attach($scope);

        $scope.subscribe('portfolios', function(){
            return [{}, null];
        });

        $scope.portfolio = portfolio;

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        }

        $scope.order = {
            code : asset ? asset.code : '',
            volume : asset ? asset.volume : 0
        };

        $scope.buy = function(){

            $scope.error = null;

            $scope.trading = true;
            Meteor.call("buyShare", $scope.order, function(error, result){

                if (error) {
                    $scope.error = error.message;
                }
                else {
                    $modalInstance.close();
                }

                $scope.trading = true;
            });
        }

        $scope.sell = function()
        {
            $scope.trading = true;
            Meteor.call("sellShare", $scope.order, function(error, result){
                if (error) {
                    console.log(error);
                }
                else {
                    $modalInstance.close();

                }
                $scope.trading = false;
            });
        }


        $scope.getStockTicker = function(){
            $scope.quoting = true;
            Meteor.call("getTicker", $scope.getReactively('order.code'), function (error, result) {
                if (result) {
                    $scope.ticker = {
                        name: result[result.length - 1].name,
                        symbol: result[result.length - 1].symbol,
                        ask: result[result.length - 1].ask
                    };
                }
                $scope.quoting = false;
            });
        };

    }
]);