angular.module('fusion').directive('portfolio', function () {
    return {
        restrict: 'E',
        templateUrl: function () {
            if (Meteor.isCordova) {
                return '/packages/fusion-mobile/client/portfolio/details.html';
            }
            else {
                return '/packages/fusion-browser/client/portfolio/details.ng.html';
            }
        },
        controllerAs: 'portfolio',
        controller: function ($scope, $state, $meteor, $location, $reactive, $modal) {
            $reactive(this).attach($scope);

            this.perPage = 5;
            this.openSellModal = function (portfolio, asset) {
                var modalInstance = $modal.open({
                    animation: true,
                    controller : 'TradeModalCtrl',
                    templateUrl: 'client/portfolio/views/trade/sell-modal.ng.html',
                    resolve: {
                        portfolio : function(){
                            return portfolio;
                        },
                        asset: function () {
                            return asset;
                        }
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            };

            this.openBuyModal = function (portfolio) {
                var modalInstance = $modal.open({
                    animation: true,
                    controller : 'TradeModalCtrl',
                    templateUrl: 'client/portfolio/views/trade/buy-modal.ng.html',
                    resolve: {
                        portfolio : function(){

                            return portfolio;
                        },
                        asset: function () {
                            return null;
                        }
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }

            this.openBalanceModal = function (isDesposit) {
                var templateUrl = isDesposit ? 'client/portfolio/views/account/deposit-modal.ng.html'
                    : 'client/portfolio/views/account/withdraw-modal.ng.html';


                var modalInstance = $modal.open({
                    animation: true,
                    controller: 'BalanceModalCtrl',
                    templateUrl: templateUrl,
                    resolve: {}
                });


                modalInstance.result.then(function () {

                });
            };


            this.helpers({
                // Subscribe to accounts and jobs
                portfolio: function () {
                    return Portfolios.findOne({owner: Meteor.userId()});
                },

                quotes : function(){
                    return Quotes.find({});
                }
            });

            this.getPrice = function(symbol, quotes){

               var cursor = quotes.filter(function(quote){
                   return quote.symbol.toUpperCase() === symbol.toUpperCase();
               });

               return cursor && cursor.length > 0 ? cursor[0].lastTradePriceOnly : 0;
            };

            this.getTotalAsset = function(assets, quotes)
            {
                var total = 0;
                var self = this;
                angular.forEach(assets, function(asset){
                    total += self.getPrice(asset.code, quotes) * asset.volume;
                });
                return total;
            };

            this.subscribe('quotes', function () {
                return [{}, null];
            });

            this.subscribe('portfolios', function () {
                return [{}, null];
            });


            this.pageChanged = function (newPage) {
                this.page = newPage;
            };
        }
    }
});
