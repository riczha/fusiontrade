angular.module('fusion').directive('watchList', function () {
    return {
        restrict: 'E',
        templateUrl: function () {
            if (Meteor.isCordova) {
                return '/packages/fusion-mobile/client/watchlist/list.html';
            }
            else {
                return '/packages/fusion-browser/client/watchlist/list.ng.html';
            }
        },
        controllerAs: 'watchlist',
        controller: function ($scope, $state, $meteor, $location, $reactive, $interval) {
            $reactive(this).attach($scope);

            this.helpers({
		        quotes : function(){
                    return Quotes.find({});
                }
            });

            this.symbol = '';
            this.searchTicker = function() {
                Meteor.call("getTicker", this.search, function (error, result) {
                    if (result) {
                        $scope.ticker = {
                            name: result[result.length - 1].name,
                            symbol: result[result.length - 1].symbol
                        };
                    }
                });
            };

            this.addIntoWatchList = function(){

                $scope.error = null;
                var code = angular.copy(this.symbol);
                this.symbol = '';
                Meteor.call('addToWatch', code, function(data, err){

                     if (err)
                     {
                         console.log(err);
                         $scope.error = err.message;
                         console.log(err);
                     }
                });
	    };


        this.subscribe('quotes', function(){
            return [{}, null];
        });


        }

    };
});
