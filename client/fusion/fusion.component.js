angular.module('fusion').directive('fusion', function () {
    return {
        restrict: 'E',
        templateUrl: function () {
            if (Meteor.isCordova) {
                return '/packages/fusion-mobile/client/fusion/base.html';
            }
            else {
                return '/packages/fusion-browser/client/fusion/base.ng.html';
            }
        },
        controllerAs: 'base',
        controller: function ($scope, $state,$meteor, $location, $reactive) {
            $reactive(this).attach($scope);

            this.helpers({
                isLoggedIn: function () {
                    return Meteor.userId() !== null;
                },
                currentUser: function () {
                    return Meteor.user();
                }
            });

            this.userName = function () {
                if (Meteor.user()) {
                    var name = Meteor.user().profile ? Meteor.user().profile.name : Meteor.user().username;
                    return name;
                }
                return 'Unknown User';
            };

            this.logout = function () {
                $meteor.logout().then(function () {
                    $state.go('login');
                }, function (error) {
                    console.log('eror' + error);
                });
            };

        }
    }
});