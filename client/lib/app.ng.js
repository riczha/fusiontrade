var modulesToLoad = [
    'angular-meteor',
    'ui.router',
    'ngStorage',
    'angularUtils.directives.dirPagination',
    'ui.bootstrap',
    'ncy-angular-breadcrumb',
    'highcharts-ng'
];

if (Meteor.isCordova) {
    modulesToLoad = modulesToLoad.concat(['fusion.mobile']);
}
else {
    modulesToLoad = modulesToLoad.concat(['fusion.browser']);
}

angular.module('fusion', modulesToLoad);

function onReady() {
    angular.bootstrap(document, ['fusion']);
}

if (Meteor.isCordova)
    angular.element(document).on("deviceready", onReady);
else
    angular.element(document).ready(onReady);