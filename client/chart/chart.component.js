angular.module('fusion').directive('chart', function () {
    return {
        restrict: 'E',
        templateUrl: function () {
            if (Meteor.isCordova) {
                return '/packages/fusion-mobile/client/chart/chart.html';
            }
            else {
                return '/packages/fusion-browser/client/chart/chart.ng.html';
            }
        },
        controllerAs: 'graphCtrl',
        controller: function ($scope, $stateParams, $reactive) {

            $reactive(this).attach($scope);

            this.symbol = $stateParams.symbol;


            $scope.loading = true;
            this.init = function(){
                Meteor.call("getHistoricalData", $stateParams.symbol, function(err, data){


                    if(data){

                        $scope.currentPrice = data[data.length-1].close.toFixed(2);

                        $scope.stock = {
                            symbol: $stateParams.symbol,
                            data: data,
                            price: $scope.currentPrice
                        };

                        $scope.series = [{
                            type: 'ohlc',
                            name: $scope.stock.name,
                            data: $scope.stock.data.map(function(item){
                                return {
                                    x: item.date,
                                    open: item.open,
                                    high: item.high,
                                    low: item.low,
                                    close: item.close
                                }
                            }),
                            tooltip: {
                                valueDecimals: 2
                            }
                        }];

                        $scope.chartConfig= {
                            rangeSelector: {
                                enabled: false
                            },
                            navigator: {
                                enabled: false
                            },
                            scrollbar: {
                                enabled: false
                            },
                            series: $scope.series
                        }

                        angular.element('#container-area').highcharts('StockChart', {
                            rangeSelector: {
                                enabled: false
                            },
                            navigator: {
                                enabled: false
                            },
                            scrollbar: {
                                enabled: false
                            },
                            series: $scope.series
                        });


                    }
                    else
                    {
                        $scope.error = err.message;
                    }

                    $scope.loading = false;
                    $scope.$digest();
                });
            }

        }
    }
})