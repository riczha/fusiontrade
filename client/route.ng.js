//This loads the initial template into the index.html page
angular.module("fusion").run(["$rootScope", '$meteor', "$location", "$state",
    function ($rootScope, $meteor, $location, $state) {


        $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
        });

        $rootScope.$on("$stateChangeError", function (event, next, previous, error) {
            //We can catch the error thrown when the $requireUser promise is rejected
            //and redirect the user back to the main page
            if (error === "AUTH_REQUIRED") {
                $state.go("login", {next: 'app.dashboard'});
            }
        });

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            //This is to not interfere with Houston Admin
        });

        $rootScope.$on('logged-in', function () {
            $state.go('app.dashboard');
        });

        $rootScope.$on('logged-out', function () {
            $state.go('login');
        });

        $rootScope.$on('bad-params', function () {
            //$state.go('trips');
        });

        //===================================================================================================================
    }]);

angular.module("fusion").config(['$urlRouterProvider', '$stateProvider', '$locationProvider', '$breadcrumbProvider',
    function ($urlRouterProvider, $stateProvider, $locationProvider, $breadcrumbProvider) {

        $locationProvider.html5Mode(true);

        $stateProvider
            .state('login', {
                url: '/login',
                template: '<login></login>'
            })
            .state('app', {
                url: '/app',
                abstract: true,
                resolve: {
                    "currentUser": ["$meteor", function ($meteor) {
                        return $meteor.requireUser();
                    }]
                },
                template: '<fusion></fusion>'
            })
            .state('app.portfolio',{
                url: '/portfolio',
                resolve : {
                    "currentUser": ["$meteor", function($meteor){
                        return $meteor.requireUser();
                    }]
                },
                template: '<portfolio></portfolio>',
                ncyBreadcrumb: {
                    label: 'My Portfolio'
                }
            })
            .state('app.watchlist',{
                url: '/watchlist',
                resolve : {
                    "currentUser": ["$meteor", function($meteor){
                        return $meteor.requireUser();
                    }]
                },
                template: '<watch-list></watch-list>',
                ncyBreadcrumb: {
                    label: 'My Watch List'
                }
            })
            .state('app.transactions',{
                url: '/transactions',
                resolve : {
                    "currentUser": ["$meteor", function($meteor){
                        return $meteor.requireUser();
                    }]
                },
                template: '<transactions></transactions>',
                ncyBreadcrumb: {
                    label: 'Transaction History'
                }
            })
            .state('app.chart',{
                url: '/chart/:symbol',
                resolve : {
                    "currentUser": ["$meteor", function($meteor){
                        return $meteor.requireUser();
                    }]
                },
                template: '<chart></chart>',
                ncyBreadcrumb: {
                    parent:'app.watchlist',
                    label: 'Historical Trend'
                }
            })
        ;

        $urlRouterProvider.otherwise("login");
    }
]);