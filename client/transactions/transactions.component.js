angular.module('fusion').directive('transactions', function () {
    return {
        restrict: 'E',
        templateUrl: function () {
            if (Meteor.isCordova) {
                return '/packages/fusion-mobile/client/transactions/list.html';
            }
            else {
                return '/packages/fusion-browser/client/transactions/list.ng.html';
            }
        },
        controllerAs: 'transactions',
        controller: function ($scope, $state, $meteor, $location, $reactive) {

            $reactive(this).attach($scope);

            this.page = 1;
            this.perPage = 10;
            this.sort = {name: 1};
            this.orderProperty = '1';

            this.search = "";

            this.helpers({
                // Subscribe to accounts and jobs
                transactions: function () {
                    return Transactions.find({});
                },
                transactionsCount: function () {
                    return Counts.get('numberOfTransactions');
                }
            });


            this.pageChanged = function (newPage) {
                this.page = newPage;
            };

            this.subscribe('transactions', function () {
                return [{
                    limit: parseInt(this.getReactively('perPage')),
                    skip: (parseInt(this.getReactively('page')) - 1) * parseInt(this.getReactively('perPage')),
                    sort: {date:-1}
                }, this.getReactively('search')];
            });

        }
    }
});